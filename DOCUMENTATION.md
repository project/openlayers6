# Documentation

* [Installation](#Installation)
* [Field rendering](#field)
* [Store locator](#store)
    - [Easy mode](#easy)
    - [Search + facets](#search)

## <a name="Installation"></a>Installation

### <a name="Requirement"></a>Requirement

* Drupal 8 or 9
* [Geofield](https://www.drupal.org/project/geofield)
* [Openlayers 6](https://github.com/openlayers/openlayers) library (choose ~6.0)
* Views rest export for store locator (built in)
* Optionally [search_api](https://www.drupal.org/project/search_api)
  , [facets](https://www.drupal.org/project/facets)for
  search needs in store locator
* Optionally whatever that can speedup
  display ([elasticsearch_connector](https://www.drupal.org/project/elasticsearch_connector)
  , [solr](https://www.drupal.org/project/search_api_solr))

Note that it does not require geojson views, but geojson format could be
implemented in near future.

### Library openlayers 6

Download [openlayers 6 (https://github.com/openlayers/openlayers/archive/refs/tags/v6.14.1.zip)](https://github.com/openlayers/openlayers/archive/refs/tags/v6.14.1.zip)
library and extract it to /path/to/drupal/libraries/openlayers.

OR

If you are using composer and asset-packagist (👍), you may use :

```bash
composer require bower-asset/openlayers:~6.14
```

### Install module

Install as you would normally install a contributed Drupal module.
Visit [documentation](https://https://www.drupal.org/node/1897420) for further
information.

## <a name="field"></a>Field rendering

Field rendering works as it would normally in fields, for further information,
visit [documentation about field rendering in drupal](https://www.drupal.org/docs/7/nodes-content-types-and-fields/specify-how-fields-are-displayed)
.

After installing this module, each geofield field type can be display as "
openlayers" format. This will display a map
centered in the location point (if many, centered by centroid) :

![](https://www.drupal.org/files/field_8.png)

### Options

* width : width of the field i.e. 100% or 200px
* height : the height
* zoom : initial zoom of the map
* icon : custom icon for markers (will be resize 40x40 max)
* tile WMS : Json options format for Tile layer :
  see [documentation](https://openlayers.org/en/latest/examples/wms-tiled.html)

Tile layer example (must be a valid json, with double quote (") attributes),
this will display a geological layer over
France :

```json
{
  "url": "http://geoservices.brgm.fr/geologie",
  "crossOrigin": "anonymous",
  "params": {
    "LAYERS": "GEOLOGIE",
    "TRANSPARENT": true,
    "TILED": true
  },
  "projection": "EPSG:4326",
  "serverType": "mapserver"
}
```

## <a name="store"></a>Store locator

1. View creation : you first have to provide a view that generate a rest export
   that can be accepted by the store
   locator block
2. Create the store locator block
3. Place it wherever you need in your website (
   using `http://yourdrupal.org/admin/structure/block` ,
   the [page_manager](https://www.drupal.org/project/page_manager) module or
   whatever)

### <a name="easy"></a>Easy mode

#### View creation

Create a view from content nodes with mode `Export REST`, this view **must**
contain 3 fields : the node ID, a latitude
and a longitude (use `Openlayers6 latitude` & `Openlayers6 longitude` to render
the geofield in your content view).
Nothing else.

Result from this view should look like :

```json
[
  {
    "id": 251,
    "lat": 41.0554,
    "lon": 2.5698
  },
  {
    "id": 258,
    "lat": 42.0554,
    "lon": 1.5698
  }
]
```

#### Create store locator block

Options available :

* **view** : you have to select the view you have just created
* **center lat**: by default, the map will be centered to this lat
* **center lon** : by default, the map will be centered to this lon
* **lat** : this is the lattitude attribute name in the REST export view you
  created (default : lat)
* **lon** : this is the longitude attribute name in the REST export view you
  created (default : lon)
* **ID** : this the ID attribute name in the REST export view you created (
  default : id)
* **display** : the display mode that will be used in markers popup
* **popup enabled** : wether you want to enable popups or not
* **cluster enabled** : wether you want to enable clusters or not
* **cluster distance** : will be the distance between clusters (
  see [example](https://openlayers.org/en/latest/examples/cluster.html))
* **zoom** : default zoom of the map
* **icon** : upload a custom icon if you need

### <a name="search"></a>Search API + facet mode

Note that you may use search, facets or both.
Same as [easy mode](#easy), create your view and then :

1. You must have the search_api and facets modules enabled.
2. In the view, you have to choose the `Facet serializer` format.
3. If you need the search feature, add a filter to the view and choose carefully
   the identifier
4. Create facet(s) using your view as a facet source. Facets must
   be `Array with raw results`, you
   must `Show the amount of results`,  and `Transform entity ID to label`. 
   `Hide facet when facet source is not rendered` should be unchecked.

**During the store locator block creation**

* Search feature : you have to fill the `Filter field machine name` that must
  match the filter identifier in the view.
* Facet feature : select facet(s) you need to display
