<?php

namespace Drupal\openlayers6\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield\Plugin\Field\FieldFormatter\GeofieldDefaultFormatter;

/**
 * Plugin implementation of the 'Longitude' formatter.
 *
 * @FieldFormatter(
 *   id = "openlayers6_longitude",
 *   label = @Translation("Openlayers6 Longitude"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class LongitudeFormatter extends GeofieldDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    if ($items->count()) {
      $item = $items->get(0);
      $geom = $this->geoPhpWrapper->load($item->value);
      $element[] = [
        '#markup' => $geom->x(),
      ];
    }
    return $element;
  }

}
