<?php

namespace Drupal\openlayers6\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\geofield\Plugin\Field\FieldFormatter\GeofieldDefaultFormatter;

/**
 * Plugin implementation of the 'openlayers6_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "openlayers6_formatter",
 *   label = @Translation("Openlayers"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class OpenlayerFormatter extends GeofieldDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => '100%',
      'height' => '400px',
      'zoom' => '10',
      'mapicon' => '',
      'tile_wms' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['width'] = [
      '#title' => $this->t('Map width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width'),
      '#required' => TRUE,
      '#description' => $this->t('Could be a fixed size i.e. 200px or a percentage i.e. 100%'),
    ];
    $form['height'] = [
      '#title' => $this->t('Map height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
      '#required' => TRUE,
      '#description' => $this->t('Could be a fixed size i.e. 200px or a percentage i.e. 100%'),
    ];
    $form['zoom'] = [
      '#title' => $this->t('Zoom level'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('zoom'),
      '#required' => TRUE,
      '#description' => $this->t('Number between 0 and 20.'),
    ];
    $form['mapicon'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Icon to render'),
      '#default_value' => $this->getSetting('mapicon'),
      '#description' => $this->t('Icon that will be placed as marker map, if none, default icon will be used.'),
      '#upload_location' => 'public://openlayers6',
      '#required' => FALSE,
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
    ];
    $form['tile_wms'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Tile WMS parameter (json format)"),
      '#description' => $this->t("If you want background layer to be different from classical OSM layer, see https://openlayers.org/en/latest/examples/wms-tiled.html"),
      '#required' => FALSE,
      '#default_value' => $this->getSetting('tile_wms') ? $this->getSetting('tile_wms') : '',
      '#element_validate' => [
        [$this, 'settingsFormWmsValidate'],
      ],
    ];
    return $form;
  }

  /**
   * Validates json if defined in settings form.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state.
   */
  public function settingsFormWmsValidate(array $element, FormStateInterface $form_state) {
    $val = $form_state->getValue($element['#parents']);
    if ($val !== '') {
      $json = json_decode($val);
      if (NULL === $json) {
        $form_state->setErrorByName("tile_wms", $this->t("Invalid JSON in Tile WSM field"));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Width: @width', ['@width' => $this->getSetting('width')]);
    $summary[] = $this->t('Height: @height', ['@height' => $this->getSetting('height')]);
    $summary[] = $this->t('Zoom: @zoom', ['@zoom' => $this->getSetting('zoom')]);
    $icon = $this->getSetting('mapicon');
    if ($icon && count($icon) && is_numeric($icon[0]) && $icon[0]) {
      $file = File::load($icon[0]);
      $summary[] = [
        'icon' => ['#markup' => $this->t("Icon to render :")],
        'image' =>
          [
            '#theme' => 'image_style',
            '#style_name' => 'thumbnail',
            '#uri' => $file->getFileUri(),
          ],
      ];
    }
    else {
      $summary[] = $this->t('Icon : no icon selected.');
    }
    $summary[] = $this->t('Tile WMS : @tile_wms', ['@tile_wms' => $this->getSetting('tile_wms')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if ($items->count()) {
      $id = Html::getUniqueId('map');
      // General settings.
      $settings = [
        'openlayers6' => [
          $id => [
            'width' => $this->getSetting('width'),
            'height' => $this->getSetting('height'),
            'zoom' => $this->getSetting('zoom'),
            // Center will be the point, if many it will be the centroid.
            'center' => ['lat' => 0, 'lon' => 0],
            'points' => [],
          ],
        ],
      ];
      // Wms layer.
      $szWMS = $this->getSetting('tile_wms');
      if ('' !== $szWMS && NULL !== $szWMS) {
        $jsonWMS = json_decode($szWMS);
        $settings['openlayers6'][$id]['wms'] = $jsonWMS;
      }
      $lats = $lons = 0;
      foreach ($items as $item) {
        $geom = $this->geoPhpWrapper->load($item->value);
        $settings['openlayers6'][$id]['points'][] = [
          'lat' => $geom->y(),
          'lon' => $geom->x(),
        ];
        $lats += $geom->y();
        $lons += $geom->x();
      }
      // Centroid calculation.
      if (count($items)) {
        $settings['openlayers6'][$id]['center']['lat'] = $lats / count($items);
        $settings['openlayers6'][$id]['center']['lon'] = $lons / count($items);
      }
      $icon = $this->getSetting('mapicon');
      $url = '';
      if ($icon && count($icon) && is_numeric($icon[0]) && $icon[0]) {
        $file = File::load($icon[0]);
        $url = \Drupal::service('file_url_generator')
          ->generateAbsoluteString($file->getFileUri());
      }
      $elements[] = [
        '#children' => sprintf('<div id="%s" style="width:%s; height: %s" data-icon="%s" data-type="openlayers6" data-processed="false"></div>',
          $id,
          htmlspecialchars($this->getSetting('width')),
          htmlspecialchars($this->getSetting('height')),
          $url),
      ];
      $elements['#attached'] = [
        'drupalSettings' => $settings,
        'library' => [
          'openlayers6/openlayers6-field',
        ],
      ];
    }
    return $elements;
  }

}
