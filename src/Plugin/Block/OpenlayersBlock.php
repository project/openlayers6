<?php

namespace Drupal\openlayers6\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\facets\Entity\Facet;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an openlayers block.
 *
 * @Block(
 *   id = "openlayers6_openlayers",
 *   admin_label = @Translation("Openlayers"),
 *   category = @Translation("Openlayers")
 * )
 */
class OpenlayersBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The facets manager.
   *
   * @var \Drupal\facets\FacetManager\DefaultFacetManager
   */
  protected $facetManager;

  /**
   * File url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * File storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * Constructs a OpenlayersBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\facets\FacetManager\DefaultFacetManager $facetManager
   *   The facet manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $moduleHandler, FileUrlGeneratorInterface $fileUrlGenerator, EntityTypeManagerInterface $entityTypeManager, ?DefaultFacetManager $facetManager = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $moduleHandler;
    $this->facetManager = $facetManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->fileStorage = $entityTypeManager->getStorage('file');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\Extension\ModuleHandler $moduleHandler */
    $moduleHandler = $container->get('module_handler');
    $facetManager = $moduleHandler->moduleExists('facets') ? $container->get('facets.manager') : NULL;

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $moduleHandler,
      $container->get('file_url_generator'),
      $container->get('entity_type.manager'),
      $facetManager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view' => 'none',
      'lat' => 'lat',
      'lon' => 'lon',
      'display' => 'teaser',
      'nid' => 'id',
      'center_lat' => '0',
      'center_lon' => '0',
      'zoom' => '10',
      'cluster_enabled' => '1',
      'popup_enabled' => '1',
      'cluster_distance' => '35',
      'filter_field' => '',
      'facets' => [],
      'facets_widget' => 'dropdown',
      'tile_wms' => '',
      'mapicon' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    /** @var \Drupal\node\Entity\NodeType[] $nodetypes */
    $views = Views::getEnabledViews();
    $options = [];
    foreach ($views as $view) {
      foreach ($view->get('display') as $displayName => $display) {
        if ($display['display_plugin'] === 'rest_export') {
          $options[$view->id()][$view->id() . ':' . $displayName] = $displayName;
        }
      }
    }
    $form['view'] = [
      '#type' => 'select',
      '#title' => $this->t('Select view'),
      '#options' => $options,
      '#default_value' => $this->configuration['view'],
      '#description' => $this->t('Select the view that provide data : node id, lat & lon must be provided'),
      '#required' => TRUE,
    ];
    $form['lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lattitude attribute'),
      '#default_value' => $this->configuration['lat'],
      '#description' => $this->t('Lattitude attribute in the rest object returned'),
      '#required' => TRUE,
    ];
    $form['lon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Longitude attribute'),
      '#default_value' => $this->configuration['lon'],
      '#description' => $this->t('Longitude attribute in the rest object returned'),
      '#required' => TRUE,
    ];
    $form['nid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID attribute'),
      '#description' => $this->t('Node ID attribute in the rest object returned'),
      '#default_value' => $this->configuration['nid'],
      '#required' => TRUE,
    ];
    $form['center_lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Center lat'),
      '#description' => $this->t('Map will be centered on this lat by default'),
      '#default_value' => $this->configuration['center_lat'],
      '#required' => TRUE,
    ];
    $form['center_lon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Center lon'),
      '#description' => $this->t('Map will be centered on this lon by default'),
      '#default_value' => $this->configuration['center_lon'],
      '#required' => TRUE,
    ];
    $form['zoom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zoom'),
      '#description' => $this->t('Zoom used by defaults'),
      '#default_value' => $this->configuration['zoom'],
      '#required' => TRUE,
    ];
    $form['popup_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Popups enabled'),
      '#default_value' => $this->configuration['cluster_enabled'] === '1' ? '1' : '0',
      '#description' => $this->t("Popup will appear when a feature is clicked."),
      '#return_value' => '1',
    ];
    $form['display'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Node's view mode to render in features popup"),
      '#description' => $this->t("When popups is enabled, Node's view mode to render in features popup, This could be 'default', 'teaser' or any custom view mode."),
      '#default_value' => $this->configuration['display'],
      '#required' => TRUE,
    ];
    $form['cluster_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clusters enabled'),
      '#default_value' => $this->configuration['cluster_enabled'] === '1' ? '1' : '0',
      '#description' => $this->t("Regroups features into cluster, you should enable this when there are lots of features in map."),
      '#return_value' => '1',
    ];
    $form['cluster_distance'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Clusters distance'),
      '#default_value' => $this->configuration['cluster_distance'],
      '#description' => $this->t("If clusters enabled, this should be a number value between 20-50 (default 35)"),
    ];
    $form['filter_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filter field machine name'),
      '#default_value' => $this->configuration['filter_field'],
      '#description' => $this->t("Search api full text filter field machine name, this will enable full text search, leaving this blank will disable search filter."),
    ];
    $form['tile_wms'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Tile WMS parameter (json format)"),
      '#description' => $this->t("If you want background layer to be different from classical OSM layer, see https://openlayers.org/en/latest/examples/wms-tiled.html"),
      '#default_value' => $this->configuration['tile_wms'] ? $this->configuration['tile_wms'] : '',
    ];
    if ($this->moduleHandler->moduleExists('facets')) {
      $facets = $this->facetManager->getFacetsByFacetSourceId('search_api:views_rest__' . str_replace(':', '__', $this->configuration['view']));
      $options = [];
      foreach ($facets as $facet) {
        $options[$facet->id()] = $facet->getName();
      }
      $form['facets'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t("Enabled facets"),
        '#options' => $options,
        '#default_value' => $this->configuration['facets'],
        '#description' => $this->t("Choosing one or more will enable facets : the view *must* use facet serializer and shows facet score."),
      ];
      $form['facets_widget'] = [
        '#type' => 'select',
        '#options' => [
          'links' => $this->t('Links'),
          'dropdown' => $this->t('Dropdown list'),
        ],
        '#default_value' => $this->configuration['facets_widget'],
      ];
    }
    $form['mapicon'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Icon to render'),
      '#default_value' => [$this->configuration['mapicon']],
      '#description' => $this->t('Icon that will be placed as marker map, if none, default icon will be used.'),
      '#upload_location' => 'public://openlayers6',
      '#required' => FALSE,
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
    ];
    return $form;
  }

  /**
   * Validates form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $wms = $form_state->getValue('tile_wms');
    if (strlen($wms)) {
      $json = json_decode($wms);
      if ($json === NULL || $json === FALSE) {
        $form_state->setErrorByName('tile_wms', $this->t("Invalid json"));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['view'] = $form_state->getValue('view');
    $this->configuration['lat'] = $form_state->getValue('lat');
    $this->configuration['lon'] = $form_state->getValue('lon');
    $this->configuration['nid'] = $form_state->getValue('nid');
    $this->configuration['display'] = $form_state->getValue('display');
    $this->configuration['center_lat'] = $form_state->getValue('center_lat');
    $this->configuration['center_lon'] = $form_state->getValue('center_lon');
    $this->configuration['zoom'] = $form_state->getValue('zoom');
    $this->configuration['popup_enabled'] = $form_state->getValue('popup_enabled');
    $this->configuration['cluster_enabled'] = $form_state->getValue('cluster_enabled');
    $this->configuration['cluster_distance'] = $form_state->getValue('cluster_distance');
    $this->configuration['filter_field'] = $form_state->getValue('filter_field');

    $mapicon = $form_state->getValue('mapicon');
    if ($mapicon && is_array($mapicon) && count($mapicon) && is_numeric($mapicon[0]) && $mapicon[0]) {
      $this->configuration['mapicon'] = $mapicon[0];
    }
    else {
      $this->configuration['mapicon'] = '';
    }

    $wms = $form_state->getValue('tile_wms');
    if (isset($wms) && strlen($wms)) {
      $wms = json_decode($wms);
      $this->configuration['tile_wms'] = json_encode($wms, JSON_UNESCAPED_SLASHES);
    }
    else {
      $this->configuration['tile_wms'] = '';
    }
    if ($form_state->hasValue('facets')) {
      $this->configuration['facets'] = $form_state->getValue('facets');
      $this->configuration['facets_widget'] = $form_state->getValue('facets_widget');
    }
    else {
      $this->configuration['facets'] = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $display = explode(':', $this->configuration['view']);
    $view = Views::getView($display[0]);
    $view->setDisplay($display[1]);
    $url = $view->getDisplay()->getUrl()->toString();
    $facetsRender = [];
    $filterRender = [];
    if ($this->configuration['filter_field']) {
      $filterRender = [
        '#theme' => 'openlayers_filter',
        '#filter_field' => $this->configuration['filter_field'],
      ];
    }
    if (count($this->configuration['facets'])) {
      $facets = Facet::loadMultiple($this->configuration['facets']);
      $facetsContent = [];
      foreach ($facets as $facet) {
        $filterKey = $facet->getFacetSourceConfig()->getFilterKey();
        $facetsSource = isset($filterKey) && $filterKey ? $filterKey : 'f';
        $theme = 'openlayers_facet';
        if (isset($this->configuration['facets_widget']) && strlen($this->configuration['facets_widget'])) {
          $theme .= '_' . $this->configuration['facets_widget'];
        }
        $facetBlock = \Drupal::service('facets.manager')->build($facet);
        $facetContent = [
          '#theme' => $theme,
          '#facet' => $facet,
          '#facetsSource' => $facetsSource,
          '#facet_block' => $facetBlock,
        ];
        if (isset($facetBlock[0]) && count($facetBlock[0])) {
          $list = reset($facetBlock[0]);
          if ($this->configuration['facets_widget'] == 'links') {
            $facetContent['#content'] = $this->computeFacetsLinks($list);
          }
          else {
            $facetContent['#content'] = $this->computeFacetDropdown($list);
          }
        }
        $facetsContent[] = $facetContent;
      }
      $facetsRender = [
        '#theme' => 'openlayers_facets',
        '#content' => $facetsContent,
      ];
    }
    $icon = '';
    if ($this->configuration['mapicon']) {
      $file = $this->fileStorage->load($this->configuration['mapicon']);
      if ($file) {
        $icon = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
      }
    }

    $build['content'] = [
      '#theme' => 'openlayers',
      '#url' => $url,
      '#url_feature' => Url::fromRoute('openlayers6.show_content_display', [
        'node' => '__NODE__',
        'display' => '__DISPLAY__',
      ])->toString(),
      '#view' => $this->configuration['view'],
      '#center_lat' => $this->configuration['center_lat'],
      '#center_lon' => $this->configuration['center_lon'],
      '#lat' => $this->configuration['lat'],
      '#lon' => $this->configuration['lon'],
      '#nid' => $this->configuration['nid'],
      '#display' => $this->configuration['display'],
      '#popup_enabled' => $this->configuration['popup_enabled'] ?? '0',
      '#cluster_enabled' => $this->configuration['cluster_enabled'] ?? '0',
      '#cluster_distance' => $this->configuration['cluster_distance'],
      '#zoom' => $this->configuration['zoom'],
      '#html_id' => Html::getUniqueId('map'),
      '#facets' => $facetsRender,
      '#filter_field' => $this->configuration['filter_field'],
      '#filter' => $filterRender,
      '#icon' => $icon,
      '#tile_wms' => $this->configuration['tile_wms'] ?? '',
      '#attached' => [
        'library' => ['openlayers6/store-locator'],
      ],
    ];
    return $build;
  }

  /**
   * Computes facet links.
   *
   * @param array $facets
   *   Array of stdClass facets.
   * @param int $depth
   *   Facets depth.
   *
   * @return array
   *   The render array.
   */
  protected function computeFacetsLinks(array $facets, $depth = 0) {
    $facetsRenderArray = [];
    foreach ($facets as $facet) {
      $content = [
        '#facet_infos' => $facet,
        'current' => [
          'current' => [
            '#type' => 'html_tag',
            '#tag' => 'li',
            '#attributes' => [
              'data-href' => $facet['url'],
              'data-count' => $facet['values']['count'],
              'data-value' => $facet['raw_value'],
              'class' => 'depth_' . $depth,
            ],
            [
              '#type' => 'html_tag',
              '#tag' => 'a',
              '#attributes' => [
                'href' => $facet['url'],
                'data-count' => $facet['values']['count'],
                'data-value' => $facet['raw_value'],
                'class' => 'depth_' . $depth,
              ],
              '#value' => $facet['values']['value'] ?? $facet['raw_value'],
            ],
          ],
        ],
      ];
      if (isset($facet['children']) && isset($facet['children'][0]) && count($facet['children'][0])) {
        $content['children'] = $this->computeFacetsLinks($facet['children'][0], $depth + 1);
      }
      $facetsRenderArray[] = $content;

    }
    return $facetsRenderArray;
  }

  /**
   * Computes facet dropdown.
   *
   * @param array $facets
   *   Array of stdClass facets.
   * @param int $depth
   *   Facets depth.
   *
   * @return array
   *   The render array.
   */
  protected function computeFacetDropdown(array $facets, $depth = 0) {
    $facetsRenderArray = [];
    foreach ($facets as $facet) {
      $content = [
        '#facet_infos' => $facet,
        'current' => [
          '#type' => 'html_tag',
          '#tag' => 'option',
          '#attributes' => [
            'value' => $facet['url'],
            'data-count' => $facet['values']['count'],
            'data-value' => $facet['raw_value'],
            'class' => 'depth_' . $depth,
          ],
          '#value' => $facet['values']['value'] ?? $facet['raw_value'],
        ],
      ];
      if (isset($facet['children']) && isset($facet['children'][0]) && count($facet['children'][0])) {
        $content['children'] = $this->computeFacetDropdown($facet['children'][0], $depth + 1);
      }
      $facetsRenderArray[] = $content;

    }
    return $facetsRenderArray;
  }

}
