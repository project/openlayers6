<?php

namespace Drupal\openlayers6\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\jsonapi\Exception\EntityAccessDeniedHttpException;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for openlayers6 routes.
 */
class OpenlayerController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs an OpenlayerController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Renderer $renderer, AccountProxyInterface $currentUser) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('current_user')
    );
  }

  /**
   * Builds the json response.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   * @param string $display
   *   (Optional) Display mode to use, default set to teaser.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The json response.
   */
  public function build(Node $node, $display = 'teaser') {
    $response = new JsonResponse();
    if ($node->access()) {
      $response = new JsonResponse();
      $viewBuilder = $this->entityTypeManager->getViewBuilder('node');
      $build = $viewBuilder->view($node, $display);
      $rendered = $this->renderer->renderRoot($build);
      $response->setContent(json_encode([
        'success' => TRUE,
        'content' => $rendered,
      ]));
    }
    else {
      throw new EntityAccessDeniedHttpException();
    }
    return $response;
  }

}
