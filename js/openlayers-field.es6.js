/**
 * @file
 * Defines Javascript behaviors for the openlayers6 module.
 */
import Map from 'ol/Map';
import {Cluster, OSM, Vector as VectorSource, TileWMS} from 'ol/source';
//import TileLayer from 'ol/layer/Tile';
import Feature from 'ol/Feature';
import View from 'ol/View';
import {fromLonLat} from 'ol/proj';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import {Icon, Style} from 'ol/style';
import Point from 'ol/geom/Point';


(function ($, Drupal, drupalSettings) {
  Drupal.openlayers_field = {
    /**
     * Icon loader
     *
     * @param iconSrc
     * @return {Promise<unknown> | Promise<unknown>}
     */
    waitForIcon: (iconSrc) => {
      return new Promise(resolve => {
        var img = new Image();
        img.addEventListener('load', () => {
          resolve(img);
        });
        if (iconSrc === "") {
          let _color = '#156BB1';
          let svg = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 25 25" xml:space="preserve">' +
            '<path fill="' + _color + '" d="M22.906,10.438c0,4.367-6.281,14.312-7.906,17.031c-1.719-2.75-7.906-12.665-7.906-17.031S10.634,2.531,15,2.531S22.906,6.071,22.906,10.438z"/>' +
            //'<image style="fill: #FFFFFF"
            // xlink:href="'+this.settings.glyphicons+'/svg/'+icondisplay+'.svg"
            // x="12" y="10" height="15" width="15"/>' +
            '<circle fill="#FFFFFF" cx="15" cy="10.677" r="3.291"/>' +
            '</svg>';
          iconSrc = 'data:image/svg+xml,' + escape(svg);
        }
        img.src = iconSrc;
      });
    }
  };

  Drupal.behaviors.openlayers_field = {
    attach(context) {
      let $context = $(context);
      $context.find('div[data-type="openlayers6"][data-processed="false"]').each(function () {
        let that = $(this);
        let _item = drupalSettings.openlayers6[that.prop('id')];
        let _features = [];
        let iconSrc = that.data('icon');
        Drupal.openlayers_field.waitForIcon(iconSrc)
          .then((evt) => {
            // image resize
            let _finalWidth = Math.min(40, evt.width);
            let _finalHeight = Math.ceil(evt.height * (_finalWidth / evt.width));
            let canvas = document.createElement('canvas');
            let ctx = canvas.getContext('2d');
            ctx.drawImage(evt, 0, 0, evt.width, evt.height, 0, 0, _finalWidth, _finalHeight)
            let dataUrl = canvas.toDataURL();
            let _finalImage = new Image();
            _finalImage.src = dataUrl;
            const icon = new Icon({
                anchor: [_finalWidth / 2, 0.5],
                anchorXUnits: 'pixels',
                anchorYUnits: 'fraction',
              img: _finalImage,
              imgSize: [_finalWidth, _finalHeight]
            })
            const iconStyle = new Style({
              image: icon,
            });
            _item.points.forEach((point) => {
              const feature = new Feature({
                //geometry: fromLonLat([point.lon, point.lat]),
                geometry: new Point(fromLonLat([point.lon, point.lat])),
                name: 'item'
              });
              feature.setStyle(iconStyle);
              _features.push(feature);
            });
            const vectorSource = new VectorSource({
              features: _features,
            });
            const vectorLayer = new VectorLayer({
              source: vectorSource,
            });
            that.width(_item.width);
            that.height(_item.height);
            let _layers = [];
            // OSM layer
            _layers.push(new TileLayer({
              source: new OSM({crossOrigin: null})
            }));
            // WMS layer if defined
            if (typeof _item.wms !== 'undefined') {
              let wmsLayer = new TileLayer({
                source: new TileWMS(_item.wms)
              });
              _layers.push(wmsLayer);
            }
            // vector layer
            _layers.push(vectorLayer);
            const map = new Map({
              layers: _layers,
              target: that.prop('id'),
              view: new View({
                center: fromLonLat([_item.center.lon, _item.center.lat]),
                zoom: _item.zoom
              }),
            });
            that.attr('data-processed', 'true');
          });
      });
    }
  }

}(jQuery, Drupal, drupalSettings));

