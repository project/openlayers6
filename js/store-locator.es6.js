import {OpenlayerMap} from "./OpenlayerMap.ts";

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.openlayers_field = {
    attach(context, settings) {
      $(context).find('div[data-map="openlayers"]:not([data-inited="true"])').each(function(){
        $(this).attr('data-inited', 'true');
        let _location = document.location.href.split("?");
        let initial = null;
        if(_location.length === 2) {
          initial = _location[1].replace(/_format=json&/, "");
        }
        let map = new OpenlayerMap($(this).prop('id'), initial);
        map.init();
      });
    }
  }
}(jQuery, Drupal, drupalSettings));
