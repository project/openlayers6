import {Map, Overlay, View} from "ol";
import {Cluster, OSM, TileWMS, Vector as VectorSource} from "ol/source";
import {fromLonLat, transformExtent} from "ol/proj";
import {defaults, FullScreen, ZoomSlider} from "ol/control";
import {defaults as defaultInteractions} from "ol/interaction";
import {Coordinate} from "ol/coordinate";
import {Tile as TileLayer, Vector as VectorLayer} from "ol/layer";
import {Fill, Icon, Stroke, Style, Text} from "ol/style";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import CircleStyle from "ol/style/Circle";
import {LineString, Polygon} from "ol/geom";
import monotoneChainConvexHull from 'monotone-chain-convex-hull';
import {createEmpty, extend, getWidth} from "ol/extent";

const $ = require("jquery");

const circleDistanceMultiplier = 1;
const circleFootSeparation = 28;
const circleStartAngle = Math.PI / 2;

const convexHullFill = new Fill({
  color: 'rgba(255, 153, 0, 0.4)',
});
const convexHullStroke = new Stroke({
  color: 'rgba(204, 85, 0, 1)',
  width: 1.5,
});
const outerCircleFill = new Fill({
  color: 'rgba(255, 153, 102, 0.3)',
});
const innerCircleFill = new Fill({
  color: 'rgba(255, 165, 0, 0.7)',
});
const textFill = new Fill({
  color: '#fff',
});
const textStroke = new Stroke({
  color: 'rgba(0, 0, 0, 0.6)',
  width: 3,
});
const innerCircle = new CircleStyle({
  radius: 14,
  fill: innerCircleFill,
});
const outerCircle = new CircleStyle({
  radius: 20,
  fill: outerCircleFill,
});

export class OpenlayerMap {
  id: string;
  el: any;
  layer: any;
  vectorLayer: any;
  interactions;
  controls;
  settings;
  center: Coordinate;
  overlay: Overlay;
  view: View;
  element;
  popup: Overlay;
  map: Map;
  global: any;
  currentUrl: string;
  loadingElement: any;
  initUrl: string;
  // icon
  szIcon: string;
  icon: any; // new Image()
  tplIcon: Icon;
  // cluster
  clusterEnabled: boolean;
  cluster: Cluster;
  hoverFeature: any;
  clusterHulls: VectorLayer<any>;
  clusterCircles: VectorLayer<any>;
  clusterDistance: number;
  clickFeature: Feature;
  clickResolution: number;
  clusterMembers: any;
  // popup
  popupContainer: any;
  popupContent: any;
  popupCloser: any;
  popupOverlay: Overlay;
  // filter
  filterInput: any;
  filterField: string;
  // facets
  facetsMapping: any = {};
  facetsMappingUrl: any = {};
  //wms
  wmsOptions: any;
  wmsLayer: TileLayer<any>;

  /**
   * @param {string} id html ID of element that contains the map.
   *   Note this element should be contained in a element with class .openlayers-wrapper.
   */
  constructor(id: string, initUrl: string = null) {
    this.id = id;
    this.el = $('#' + this.id)
    this.global = {};
    this.filterInput = $('.openlayers-filters input');
    this.szIcon = this.el.data('icon');
    this.filterField = this.el.data('filter-field');
    this.popupContainer = this.el.closest('.openlayers-wrapper').find('.popup');
    this.popupContent = this.el.closest('.openlayers-wrapper').find('.popup-content');
    this.popupCloser = this.el.closest('.openlayers-wrapper').find('.popup-closer');
    this.loadingElement = this.el.closest('.openlayers-wrapper').find('.openlayers-loading-wrapper');
    this.initUrl = initUrl;
  }

  /**
   * Initializes map.
   */
  init(): void {
    let extent = [40428, 1600797, 1216896, 2698833.8];
    const _layers = [];
    this.layer = new TileLayer({
      source: new OSM({crossOrigin: 'anonymous'})
    });
    _layers.push(this.layer);
    if (typeof this.el.data('tile-wms') !== 'undefined' && typeof this.el.data('tile-wms') === "object") {
      this.wmsLayer = new TileLayer({
        source: new TileWMS(this.el.data('tile-wms'))
      });
      _layers.push(this.wmsLayer);
    }

    this.interactions = defaultInteractions();
    this.controls = defaults();
    this.controls.push(new FullScreen());
    this.controls.push(new ZoomSlider());
    this.center = fromLonLat([parseFloat(this.el.data('center-lon')), parseFloat(this.el.data('center-lat'))]);
    this.clusterEnabled = this.el.data('cluster-enabled') == '1';
    this.clusterDistance = parseInt(this.el.data('cluster-distance'));
    this.view = new View({
      center: this.center,
      zoom: this.el.data('zoom'), // zoom goes here
      minZoom: 1,
      maxZoom: 18
    });
    this.popupOverlay = new Overlay({
      element: this.popupContainer.get(0),
      positioning: 'bottom-center',
      stopEvent: true,
    });
    this.map = new Map({
      target: this.id,
      layers: _layers,
      interactions: this.interactions,
      controls: this.controls,
      //overlays: [this.overlay],
      view: this.view
    });
    this.map.addOverlay(this.popupOverlay);
    this.loadData();
  }

  /**
   * Single feature style, users for clusters with 1 feature and cluster circles.
   * @param {Feature} clusterMember A feature from a cluster.
   * @return {Style} An icon style for the cluster member's location.
   */
  clusterMemberStyle(clusterMember: Feature): Style {
    return new Style({
      geometry: clusterMember.getGeometry(),
      image: this.getIcon(),
    });
  }

  /**
   * Loads data from the API (drupal view)
   *
   * @param {string} url url to be called.
   */
  loadData(url: string = null): void {
    var bInit = false;
    if(null != this.initUrl) {
      url = this.el.data('url') + "?_format=json&" + this.initUrl;
      this.initUrl = null;
      bInit = true;
    }
    this.el.trigger('openlayers_beforeloaddata', [this, url]);
    let that = this;
    that.hideFeaturePopup();
    let extent = this.map.getView().calculateExtent(this.map.getSize());
    if (url === null) {
      url = this.el.data('url') + '?_format=json&view=' + this.el.data('view');
    }

    if(!bInit) {
      let reg = new RegExp("&openlayers-searchtext=[^&]*(&|$)")
      url = url.replace(reg, '&');
      if (this.filterField !== '' && this.filterInput.val() !== '' && this.filterInput.length > 0) {
        url += '&openlayers-searchtext=' + this.filterInput.val();
      }
    }
    url = url.replace(/^https?:/, '')
    this.currentUrl = url;
    // @todo coordinates should be used to select only required features.
    extent = transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
    this.loadingElement.show();
    let _attrLat = this.el.data('lat');
    let _attrLon = this.el.data('lon');
    let _attrID = this.el.data('nid');
    fetch(this.currentUrl, {
        method: 'get',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      }
    ).then((response) => {
      this.waitForIcon(this.szIcon).then((evt) => {

      response.json().then((json) => {
        $(this.global).trigger('resultReceived', [{"json": json}]);
        let _features = [];
        const iconStyle = new Style({
          image: this.getIcon(evt),
        });
        let result = json;
        if (typeof json.search_results !== 'undefined') {
          result = json.search_results;
        }
        try {
          result.forEach((point) => {
            const feature = new Feature({
              geometry: new Point(fromLonLat([point[_attrLon], point[_attrLat]])),
              name: 'item',
              properties: {id: point[_attrID]},
            });
            feature.setStyle(iconStyle);
            _features.push(feature);
          });
          if (typeof this.vectorLayer !== "undefined") {
            this.map.removeLayer(this.vectorLayer);
          }
          const vectorSource = new VectorSource({
            features: _features,
          });
          if (this.clusterEnabled) {
            this.cluster = new Cluster({
              distance: this.clusterDistance,
              source: vectorSource
            });
            this.clusterHulls = new VectorLayer({
              source: this.cluster,
              style: this.clusterHullStyle,
            });
            this.vectorLayer = new VectorLayer({
              source: this.cluster,
              style: function (feature) {
                const size = feature.get('features').length;
                if (size > 1) {
                  return [
                    new Style({
                      image: outerCircle,
                    }),
                    new Style({
                      image: innerCircle,
                      text: new Text({
                        text: size.toString(),
                        fill: textFill,
                        stroke: textStroke,
                      }),
                    }),
                  ];
                } else {
                  const originalFeature = feature.get('features')[0];
                  return that.clusterMemberStyle(originalFeature);
                }
              },
            });
            this.clusterCircles = new VectorLayer({
              source: this.cluster,
              style: this.clusterCircleStyle,
            });
          } else {
            this.vectorLayer = new VectorLayer({
              source: vectorSource,
            });
          }
          this.map.addLayer(this.vectorLayer);
        }catch(e) {
          if (typeof this.vectorLayer !== "undefined") {
            this.map.removeLayer(this.vectorLayer);
          }
        }
        this.el.trigger('openlayers_afterloaddata', [this, url, json]);
      })});
      this.poseListeners(true);
      this.loadingElement.hide();
    });
  }

  /**
   * Icon loader
   *
   * @param iconSrc
   * @return {Promise<unknown> | Promise<unknown>}
   */
  waitForIcon(iconSrc:string = ""): Promise<any> {
    let that = this;
    return new Promise(resolve => {
      that.icon = new Image();
      that.icon.addEventListener('load', () => {
        resolve(that.icon);
      });
      if (iconSrc === "") {
        let _color = '#156BB1';
        let svg = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 25 25" xml:space="preserve">' +
          '<path fill="' + _color + '" d="M22.906,10.438c0,4.367-6.281,14.312-7.906,17.031c-1.719-2.75-7.906-12.665-7.906-17.031S10.634,2.531,15,2.531S22.906,6.071,22.906,10.438z"/>' +
          '<circle fill="#FFFFFF" cx="15" cy="10.677" r="3.291"/>' +
          '</svg>';
        iconSrc = 'data:image/svg+xml,' + escape(svg);
      }
      that.icon.src = iconSrc;
    });
  }

  /**
   * Style for clusters with features that are too close to each other, activated on click.
   * @param {Feature} cluster A cluster with overlapping members.
   * @param {number} resolution The current view resolution.
   * @return {Style} A style to render an expanded view of the cluster members.
   */
  clusterCircleStyle(cluster: Feature, resolution: number) {
    if (cluster !== this.clickFeature || resolution !== this.clickResolution) {
      return;
    }
    this.clusterMembers = cluster.get('features');
    let centerPoint: Point = <Point>cluster.getGeometry();
    const centerCoordinates = centerPoint.getCoordinates();
    let clusterPoint: Point = <Point>cluster.getGeometry();
    return this.generatePointsCircle(
      this.clusterMembers.length,
      clusterPoint.getCoordinates(),
      resolution
    ).reduce((styles, coordinates, i) => {
      const point = new Point(coordinates);
      const line = new LineString([centerCoordinates, coordinates]);
      styles.unshift(
        new Style({
          geometry: line,
          stroke: convexHullStroke,
        })
      );
      styles.push(
        this.clusterMemberStyle(
          new Feature({
            ...this.clusterMembers[i].getProperties(),
            geometry: point,
          })
        )
      );
      return styles;
    }, []);
  }

  /**
   * From
   * https://github.com/Leaflet/Leaflet.markercluster/blob/31360f2/src/MarkerCluster.Spiderfier.js#L55-L72
   * Arranges points in a circle around the cluster center, with a line pointing from the center to
   * each point.
   * @param {number} count Number of cluster members.
   * @param {Array<number>} clusterCenter Center coordinate of the cluster.
   * @param {number} resolution Current view resolution.
   * @return {Array<Array<number>>} An array of coordinates representing the cluster members.
   */
  generatePointsCircle(count, clusterCenter, resolution): any {
    const circumference =
      circleDistanceMultiplier * circleFootSeparation * (2 + count);
    let legLength = circumference / (Math.PI * 2); //radius from circumference
    const angleStep = (Math.PI * 2) / count;
    const res = [];
    let angle;
    legLength = Math.max(legLength, 35) * resolution; // Minimum distance to get outside the cluster icon.
    for (let i = 0; i < count; ++i) {
      // Clockwise, like spiral.
      angle = circleStartAngle + i * angleStep;
      res.push([
        clusterCenter[0] + legLength * Math.cos(angle),
        clusterCenter[1] + legLength * Math.sin(angle),
      ]);
    }
    return res;
  }

  /**
   *
   * @param cluster
   */
  clusterHullStyle(cluster): Style {
    if (cluster !== this.hoverFeature) {
      return;
    }
    const originalFeatures = cluster.get('features');
    const points = originalFeatures.map((feature) =>
      feature.getGeometry().getCoordinates()
    );
    return new Style({
      geometry: new Polygon([monotoneChainConvexHull(points)]),
      fill: convexHullFill,
      stroke: convexHullStroke,
    });
  }

  /**
   * Returns Icon for feature.
   */
  getIcon(evt:any = null): Icon {
    if(this.tplIcon === null || typeof this.tplIcon === 'undefined' || evt !== null) {
      let _finalWidth = Math.min(40, evt.width);
      let _finalHeight = Math.ceil(evt.height * (_finalWidth / evt.width));
      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');
      ctx.drawImage(evt, 0, 0, evt.width, evt.height, 0, 0, _finalWidth, _finalHeight)
      let dataUrl = canvas.toDataURL();
      let _finalImage = new Image();
      _finalImage.src = dataUrl;
      this.tplIcon  = new Icon({
        anchor: [_finalWidth / 2, 0.5],
        anchorXUnits: 'pixels',
        anchorYUnits: 'fraction',
        img: _finalImage,
        imgSize: [_finalWidth, _finalHeight]
      })
    }
    return this.tplIcon;
  }

  /**
   * Pose all listeners on the map.
   *
   * @param {boolean} _b pose|unpose
   */
  poseListeners(_b: boolean = true): void {
    if (_b) {
      let that = this;
      this.poseListeners(false);
      $(this.global).on('resultReceived', function (evt, data) {
        that.resultReceivedHandler.apply(that, [evt, data])
      });
    } else {
      $(this.global).off('resultReceived');
    }
    this.enableMapClick(_b);
    this.enableFilterChange(_b);
  }

  /**
   * Ensure vectorLayer exists for event puprose.
   */
  vectorLayerReady(): Promise<any> {
    let that = this;
    return new Promise(function (resolve, reject) {
      that.waitForVectorLayer(resolve);
    });
  }

  /**
   * Waits for vectorLayer to be defined.
   *
   * @param resolve
   */
  waitForVectorLayer(resolve) {
    if (typeof this.vectorLayer === 'undefined') {
      setTimeout(this.waitForVectorLayer.bind(this, resolve), 200);
    } else {
      resolve();
    }
  }

  /**
   *
   * @param {boolean} _b enable /disable
   */
  enableMapClick(_b: boolean = true): void {
    let that = this;
    this.vectorLayerReady().then(() => {
      if (this.clusterEnabled) {
        this.map.on('click', (event) => {
          this.vectorLayer.getFeatures(event.pixel).then((features) => {
            this.clusterMembers = [];
            if (features.length > 0) {
              this.clusterMembers = features[0].get('features');
            }
            // This is a cluster click
            if (typeof this.clusterMembers !== 'undefined' && this.clusterMembers.length > 1) {
              // Calculate the extent of the cluster members.
              const extent = createEmpty();
              this.clusterMembers.forEach((feature) =>
                extend(extent, feature.getGeometry().getExtent())
              );
              const view = this.map.getView();
              const resolution = this.map.getView().getResolution();
              if (
                view.getZoom() === view.getMaxZoom() ||
                (getWidth(extent) < resolution && getWidth(extent) < resolution)
              ) {
                // Show an expanded view of the cluster members.
                this.clickFeature = features[0];
                this.clickResolution = resolution;
                this.clusterCircles.setStyle(this.clusterCircleStyle);
              } else {
                // Zoom to the extent of the cluster members.
                view.fit(extent, {duration: 500, padding: [50, 50, 50, 50]});
              }
              //This is an icon click
            } else if (typeof this.clusterMembers !== 'undefined' && this.clusterMembers.length === 1) {
              this.showFeaturePopup(this.clusterMembers[0]);
            } else {
              // click random place in map
            }
          });
        });
        this.map.on('pointermove', (event) => {
          if (this.clusterEnabled) {
            this.vectorLayer.getFeatures(event.pixel).then((features) => {
              if (features.length > 0 && features[0] !== this.hoverFeature) {
                // Display the convex hull on hover.
                this.hoverFeature = features[0];
                this.clusterHulls.setStyle(this.clusterHullStyle);
                // Change the cursor style to indicate that the cluster is clickable.
                this.map.getTargetElement().style.cursor =
                  this.hoverFeature && this.hoverFeature.get('features').length > 1 ? 'pointer' : '';
              }
            });
          }
          let _f = this.map.forEachFeatureAtPixel(event.pixel, (_feature) => {
            return _feature;
          });
          this.map.getTargetElement().style.cursor = (typeof _f !== 'undefined') ? 'pointer': '';
        });
      } else {
        this.map.on('click', (event) => {
          this.vectorLayer.getFeatures(event.pixel).then((features) => {
            this.clusterMembers = [];
            if (features.length === 1) {
              this.showFeaturePopup(features[0]);
            }
          });
        });
      }
      this.popupCloser.on('click', (evt) => {
        evt.stopPropagation();
        that.hideFeaturePopup();
        return false;
      });
    });
  }

  /**
   * Hides feature popup.
   */
  hideFeaturePopup(): void {
    this.popupOverlay.setPosition(undefined);
    this.popupCloser.get(0).blur();
  }

  /**
   * Enables filter field change
   *
   * @param _b
   */
  enableFilterChange(_b: boolean): void {
    if (this.filterField !== '' && this.filterInput.length > 0) {
      let that = this;
      if (_b) {
        this.filterInput.on('change', function (evt) {
          that.loadData.apply(that, [that.currentUrl]);
        });
      } else {
        this.filterInput.off('change');
      }
    }
  }

  /**
   * shows feature popup.
   *
   * @param {Feature} feature
   */
  showFeaturePopup(feature: Feature): void {
    // getGeometry().getCoordinates() will lead to compilation error as we use ts.
    // @see https://stackoverflow.com/questions/37488179/how-can-i-get-the-coordinates-of-a-feature-in-openlayers-3
    let geom = <Point>feature.getGeometry(); //.transform('EPSG:3857', 'EPSG:4326');
    let coords = geom.getCoordinates();
    let properties = feature.get('properties');
    let content = '<div id="feature-' + properties.id + '"><p style="text-align:center;"> ... loading ...</p></div>'

    this.popupContent.html(content);
    this.popupOverlay.setPosition(coords);
    $('.ol-popup').css('pointer-events', 'all')
    let _url = this.el.data('url-feature').replace('__NODE__', properties.id).replace('__DISPLAY__', this.el.data('view-mode'));
    fetch(_url, {
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
      }
    ).then((response) => {
      response.json().then((json) => {
        $('#feature-' + properties.id).html(json.content);
        $('#feature-' + properties.id + 'a').on('click', (evt) => {
          if($(this).prop('href') !== '#') {
            document.location.href = $(this).prop('href');
          }
        });
      })
    });
  }

  /**
   * Event dispatched on received data from drupal view, before rendering marker.
   *
   * @param evt native jquery event
   * @param data object with json received
   */
  resultReceivedHandler(evt, data): void {
    if (typeof data.json.facets_metadata !== 'undefined') {
      for (let [facetId, facetMetadata] of Object.entries(data.json.facets_metadata)) {
        let _el = $('.openlayers-facets div#' + facetId);
        let _facetMetadata: any = facetMetadata;
        this.facetsMapping[_facetMetadata.field_id] = facetId;
        this.facetsMappingUrl[_facetMetadata.field_id] = _facetMetadata.url_alias;
      }
    }
    if (typeof data.json.facets !== 'undefined') {
      this.fillFacets(data.json.facets);
    }
    this.enableFacetLinkHandler(true);
  }

  /**
   * Gets the link the API will use to reset a facet.
   *
   * @param {string} facetId facet id to reset
   * @param {$} el jQuery element that contains facet
   *
   * @return {string} Recycle facet url.
   */
  recycleUrlFromFacet(facetId: string, el): string {
    let queryString: string = this.currentUrl.split('?')[1];
    let params = new URLSearchParams(queryString);
    let recycleParams = new URLSearchParams({});
    let num = 0;
    let facetReg = new RegExp("^" + el.data('key') + "\\[");
    params.forEach((value: string, key: string) => {
      let reg = new RegExp('^' + this.facetsMappingUrl[facetId] + ':');
      if (!key.match(facetReg)) {
        recycleParams.set(key, value);
      } else if (key.match(facetReg) && value.match(reg) === null) {
        recycleParams.set(el.data('key') + '[' + num + ']', value);
        num++;
      }
    });
    return this.el.data('url') + '/?' + recycleParams.toString();
  }

  /**
   * Enable links behaviors.
   *
   * @param {boolean} _b enable/disable
   */
  enableFacetLinkHandler(_b: boolean = true): void {
    if (_b) {
      let that = this;
      this.enableFacetLinkHandler(false);
      $('.openlayers-facets a').on('click', function (evt) {
        return that.facetLinkClickedHandler.apply(that, [evt, $(this)]);
      });
      $('.openlayers-facets select').on('change', function (evt) {
        return that.facetSelectChangedHandler.apply(that, [evt, $(this)]);
      });
    } else {
      $('.openlayers-facets a').off('click');
      $('.openlayers-facets select').off('change');
    }
  }

  /**
   * Called on facet link clicked.
   *
   * @param evt
   * @param link
   */
  facetLinkClickedHandler(evt, link): boolean {
    this.enableFacetLinkHandler(false);
    this.loadData(link.prop('href'));
    return false;
  }

  /**
   * Called on facet select changed.
   *
   * @param evt
   * @param select
   */
  facetSelectChangedHandler(evt, select): boolean {
    this.enableFacetLinkHandler(false);
    this.loadData(select.val());
    return false;
  }



  /**
   * Fills facets links
   *
   * Facets are firstly server generated, just filtered here
   * By hiding/showing the one that have count > 1 values.
   *
   * @param facetResponse
   */
  fillFacets(facetResponse): void {
    let queryString: string = this.currentUrl.split('?')[1];
    let params = new URLSearchParams(queryString);
    this.el.trigger('openlayers_beforefacetresponse', [this, facetResponse, params]);
    $('.openlayers-facets li').hide();
    $('.openlayers-facets option').not('[value=""]').hide();
    for (let i in facetResponse) {
      for (let [facetIds, facetsObject] of Object.entries(facetResponse[i])) {
        let _facetsObject: any = facetsObject;
        for (let facetId in _facetsObject) {
          let facets: any = _facetsObject[facetId];
          this.fillFacetsRecurse(facets, params);
        }
      }
    }
    this.el.trigger('openlayers_afterfacetresponse', [this, facetResponse, params]);
  }

  /**
   *
   *
   * @param facets
   * @param params
   */
  fillFacetsRecurse(facets:any, params:any) : void {
    for (let k in facets) {
      let item: any = facets[k];
      if (typeof item.raw_value != 'undefined') {
        let reg = new RegExp(item.raw_value + '$');
        let selectedClass = false;
        params.forEach((value: string, key: string) => {
          if (value.match(reg) !== null) {
            selectedClass = true;
          }
        });
        let _option = $('.openlayers-facets option[data-value="'+ item.raw_value +'"]');
        let _li = $('.openlayers-facets li[data-value="'+ item.raw_value +'"]');
        let _a = $('.openlayers-facets li[data-value="'+ item.raw_value +'"] a');
        _option.attr('data-count', item.values.count).attr('value', item.url).removeClass('selected').show();
        _li.attr('data-count', item.values.count).attr('data-href', item.url).removeClass('selected').show();
        _a.attr('data-count', item.values.count).attr('href', item.url).removeClass('selected').show();
        if(selectedClass) {
          _option.addClass('selected');
          _li.addClass('selected');
          _a.addClass('selected');
        }
        if (typeof item.children !== 'undefined' && typeof item.children[0] !== 'undefined' && item.children[0].length > 0) {
          this.fillFacetsRecurse(item.children[0], params);
        }
      }
    }
  }
}
