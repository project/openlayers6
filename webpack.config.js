const path = require('path');
const glob = require('glob');

// ES6 JS in the custom Drupal modules.
const modulePath = path.resolve(__dirname);
const matchesInModules = glob.sync(`${modulePath}/js/*.es6.js`);
const entry = {};


matchesInModules.forEach((match) => {
  entry[match] = match;
});

/**
 * Gets the file path of the compiled JS in the Drupal module.
 *
 * The compiled module JS files stay in the same directory of source js files.
 * The new path will be generated based on /path/to/module/js/*.es6.js, and will
 * be /path/to/module/js/*.js.
 *
 * @param filename
 * @returns {string}
 */
const moduleJsOutputPath = (filePath) => {
  if (filePath.slice(-7) === '.es6.js') {
    const fileName = path.basename(filePath);
    const fileDir = path.dirname(filePath);
    const relativeDir = path.relative(modulePath, fileDir);
    // Remove .es6.js file extension.
    const newFileName = fileName.slice(0, -7);
    const newFilePath = `${relativeDir}/${newFileName}.js`;
    return newFilePath;
  }
};


module.exports = {
  entry: entry,
  resolve: {
    extensions: ['.js', '.ts']
  },
  output: {
    path: modulePath,
    filename(object) {
      const filePath = object.chunk.name;
      let newFilePath;
      newFilePath = moduleJsOutputPath(filePath);
      return newFilePath;
    },
  },
  devtool: 'hidden-source-map',
  externals: {
    jquery: 'jQuery',
    Drupal: 'Drupal',
    drupalSettings: 'drupalSettings',
  },
  module: {
    rules: [
      {
        test: /\.(es6\.js)/,
        include: [new RegExp(modulePath)],
        loader: 'babel-loader',
      },
      {
        test: /\.ts/,
        include: [new RegExp(modulePath)],
        loader: 'ts-loader',
      }
    ],
  }
};
