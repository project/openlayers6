# OpenLayers6 module

This module provides **openlayers field renderer** for **geofield**.
It also provide a **store locator** over a openlayer map : datasource is
provided by the view module and search_api.

Datasource could be filtered using the facets module.

See [documentation here](DOCUMENTATION.md).
