<?php

namespace Drupal\Tests\openlayers6\Functional;

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\views\Views;

/**
 * Trait for openlayers6 functional tests.
 */
trait Openlayers6TestTrait {

  use StringTranslationTrait;

  /**
   * A field storage with cardinality 1 to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * A Field to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * Setups env.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setupEnv() {
    $this->createEntityWithFieldLocation();
  }

  /**
   * Creates admin user and login.
   *
   * @return \Drupal\user\Entity\User|false
   *   The created user.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createAdminUserAndLogin() {
    $user = $this->drupalCreateUser([
      'access administration pages',
      'access content',
      'create page content',
      'administer node display',
      'administer node fields',
      'administer nodes',
      'administer blocks',
      'administer themes',
    ]);
    $this->drupalLogin($user);
    return $user;
  }

  /**
   * Setups render field location.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUpRenderFieldLocation() {
    $this->createAdminUserAndLogin();
    $this->drupalGet('admin/structure/types/manage/page/display');
    $this->assertSession()
      ->elementExists('xpath', '//select[@name="fields[location][type]"]/option[@value="openlayers6_formatter"]');
    $this->getSession()
      ->getPage()
      ->selectFieldOption('fields[location][type]', 'openlayers6_formatter');
    $this->getSession()
      ->getPage()
      ->selectFieldOption('fields[location][region]', 'content');
    $this->getSession()->getPage()->pressButton('Save');
    $this->assertSession()->pageTextContains('Your settings have been saved.');
  }

  /**
   * Creates located nodes.
   *
   * @return \Drupal\node\Entity\Node[]
   *   Array of nodes created.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function createLocatedNodes($num = 10) {
    $geoService = \Drupal::service('geofield.wkt_generator');
    $nodes = [];
    for ($i = 0; $i < $num; $i++) {
      $point = ['lat' => rand(0, 50), 'lon' => rand(0, 8)];
      $value = $geoService->WktBuildPoint($point);
      $node = $this->drupalCreateNode([
        'type' => 'page',
        'title' => 'Somewhere on the earth',
      ]);
      $node->location->setValue($value);
      $node->save();
      $nodes[] = $node;
    }
    return $nodes;
  }

  /**
   * Creates the entity with field location inside.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createEntityWithFieldLocation() {
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'page',
    ]);
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => 'location',
      'entity_type' => 'node',
      'type' => 'geofield',
      'settings' => [
        'backend' => 'geofield_backend_default',
      ],
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'page',
      'description' => 'Description for geofield_field',
      'settings' => [
        'backend' => 'geofield_backend_default',
      ],
      'required' => TRUE,
    ]);
    $this->field->save();
  }

  /**
   * Reads configuration files.
   *
   * @param string $id
   *   Config Id.
   * @param string $storageDir
   *   Storage dir.
   *
   * @return mixed
   *   Some configuration.
   */
  public function readConfig($id, $storageDir = InstallStorage::CONFIG_OPTIONAL_DIRECTORY) {
    $dir = \Drupal::service('module_handler')
      ->getModule('openlayers6')
      ->getPath();
    $storage = new FileStorage($dir . '/' . $storageDir);
    return $storage->read($id);
  }

  /**
   * Creates view.
   *
   * @return \Drupal\views\ViewExecutable|null
   *   The view.
   */
  public function createViewForStoreLocator() {
    $view = $this->readConfig('views.view.storelocatortest');
    \Drupal::configFactory()->getEditable('views.view.storelocatortest')
      ->setData($view)
      ->save(TRUE);
    $view = Views::getView('storelocatortest');
    $this->assertNotNull($view);
    return $view;
  }

}
