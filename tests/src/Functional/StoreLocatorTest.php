<?php

namespace Drupal\Tests\openlayers6\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group openlayers6
 */
class StoreLocatorTest extends BrowserTestBase {

  use Openlayers6TestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'openlayers6',
    'node',
    'field',
    'field_ui',
    'block',
    'filter',
    'views',
    'serialization',
    'rest',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupEnv();
    $this->createViewForStoreLocator();
    drupal_flush_all_caches();
  }

  /**
   * Test callback.
   */
  public function testFieldRendering() {
    $this->createAdminUserAndLogin();
    $this->createLocatedNodes();
    $this->drupalGet('admin/structure/block');
    $this->getSession()
      ->getPage()
      ->clickLink('Place block in the Content region');
    $this->assertSession()
      ->elementExists('xpath', '//a[contains(@href, "/admin/structure/block/add/openlayers6_openlayers/claro?region=content")]');
  }

}
