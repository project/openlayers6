<?php

namespace Drupal\Tests\openlayers6\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group openlayers6
 */
class FieldRenderTest extends BrowserTestBase {

  use Openlayers6TestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'openlayers6',
    'node',
    'field',
    'field_ui',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupEnv();
  }

  /**
   * Test callback.
   */
  public function testFieldRendering() {
    $this->setUpRenderFieldLocation();
    $point = ['lat' => 42, 'lon' => 2];
    $value = \Drupal::service('geofield.wkt_generator')->WktBuildPoint($point);
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'title' => 'Lost on the earth',
    ]);
    $this->drupalGet($node->toUrl());
    $this->assertSession()
      ->elementNotExists('xpath', '//div[@data-type="openlayers6"]');
    $node->location->setValue($value);
    $node->save();
    $this->drupalGet($node->toUrl());
    $this->assertSession()
      ->elementExists('xpath', '//div[@data-type="openlayers6"]');

  }

}
